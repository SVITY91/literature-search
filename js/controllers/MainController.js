function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function getColorRGB(mix) {

    var red = getRandomInt(0, 255);
    var green = getRandomInt(0, 255);
    var blue = getRandomInt(0, 255);

    if(mix !== undefined)
    {
        red = (red + mix[0])/2;
        green = (green + mix[1])/2;
        blue = (blue + mix[2])/2;
    }

    return "rgb(" + parseInt(red) + ", " + parseInt(green) + ", " + parseInt(blue) + ")";
}

app.controller('MainController', ['$scope', '$http' , function ($scope, $http) {


    console.log('MainController...');

    $scope.searchString = '';

    $scope.$watch('searchString', function(newValue, oldValue){
        console.log(newValue);
        if(newValue.length == 0){
            $scope.books = [];

            return;
        }
        search();
    });

    $scope.isSearchActive = function(){
        return $scope.books.length != 0;
    };

    function search(){

        $scope.id = $scope.searchString + Date.now();

        $http({
            method: 'POST',
            url: 'search.php',
            data: {
                'query': $scope.searchString,
                'id': $scope.id
            }
        })
            .success(function (data) {

                if($scope.id != data.id) {
                    console.log('ID is bad!!!');
                    return;
                }

                console.log(data.query);

                $scope.books = [
                    {
                        "BOOK_AUTHOR": "Erinna Edeline",
                        "BOOK_DATE": "16 June 1958",
                        "BOOK_DESCRIPTION": null,
                        "BOOK_ID": 0,
                        "BOOK_LANGUAGE": "German",
                        "BOOK_NAME": "Bethune Rozamond",
                        "BOOK_PATH": null
                    },{
                        "BOOK_AUTHOR": "Lynde Karina",
                        "BOOK_DATE": "6 April 1877",
                        "BOOK_DESCRIPTION": null,
                        "BOOK_ID": 0,
                        "BOOK_LANGUAGE": "Russian",
                        "BOOK_NAME": "Elsa Barbie",
                        "BOOK_PATH": null
                    },{
                        "BOOK_AUTHOR": "Minnnie Janot",
                        "BOOK_DATE": "8 July 1861",
                        "BOOK_DESCRIPTION": null,
                        "BOOK_ID": 0,
                        "BOOK_LANGUAGE": "German",
                        "BOOK_NAME": "Floriston Erinn",
                        "BOOK_PATH": null
                    },{
                        "BOOK_AUTHOR": "Brita Silvie",
                        "BOOK_DATE": "14 February 1994",
                        "BOOK_DESCRIPTION": null,
                        "BOOK_ID": 0,
                        "BOOK_LANGUAGE": "Russian",
                        "BOOK_NAME": "Pelsor Robena",
                        "BOOK_PATH": null
                    },{
                        "BOOK_AUTHOR": "Trixy Jennifer",
                        "BOOK_DATE": "25 November 1901",
                        "BOOK_DESCRIPTION": null,
                        "BOOK_ID": 0,
                        "BOOK_LANGUAGE": "English",
                        "BOOK_NAME": "Tamaqua Niki",
                        "BOOK_PATH": null
                    },{
                        "BOOK_AUTHOR": "Eada Jacky",
                        "BOOK_DATE": "9 February 1844",
                        "BOOK_DESCRIPTION": null,
                        "BOOK_ID": 0,
                        "BOOK_LANGUAGE": "English",
                        "BOOK_NAME": "Eddyville Elnora",
                        "BOOK_PATH": null
                    },{
                        "BOOK_AUTHOR": "Bellanca Daniele",
                        "BOOK_DATE": "10 April 1922",
                        "BOOK_DESCRIPTION": null,
                        "BOOK_ID": 0,
                        "BOOK_LANGUAGE": "English",
                        "BOOK_NAME": "Edmeston Darda",
                        "BOOK_PATH": null
                    },{
                        "BOOK_AUTHOR": "Rayna Cordi",
                        "BOOK_DATE": "7 November 1988",
                        "BOOK_DESCRIPTION": null,
                        "BOOK_ID": 0,
                        "BOOK_LANGUAGE": "German",
                        "BOOK_NAME": "Enochs Rachelle",
                        "BOOK_PATH": null
                    },{
                        "BOOK_AUTHOR": "Addy Shandy",
                        "BOOK_DATE": "11 February 1867",
                        "BOOK_DESCRIPTION": null,
                        "BOOK_ID": 0,
                        "BOOK_LANGUAGE": "Chinese",
                        "BOOK_NAME": "Postelle Melodie",
                        "BOOK_PATH": null
                    },{
                        "BOOK_AUTHOR": "Alethea Gerianne",
                        "BOOK_DATE": "12 July 1909",
                        "BOOK_DESCRIPTION": null,
                        "BOOK_ID": 0,
                        "BOOK_LANGUAGE": "Russian",
                        "BOOK_NAME": "Noctor Sarena",
                        "BOOK_PATH": null
                    },{
                        "BOOK_AUTHOR": "Malina Edythe",
                        "BOOK_DATE": "4 December 1837",
                        "BOOK_DESCRIPTION": null,
                        "BOOK_ID": 0,
                        "BOOK_LANGUAGE": "German",
                        "BOOK_NAME": "Tams Andriette",
                        "BOOK_PATH": null
                    },{
                        "BOOK_AUTHOR": "Imogene Emmye",
                        "BOOK_DATE": "9 June 1852",
                        "BOOK_DESCRIPTION": null,
                        "BOOK_ID": 0,
                        "BOOK_LANGUAGE": "Russian",
                        "BOOK_NAME": "Lilydale Sacha",
                        "BOOK_PATH": null
                    },{
                        "BOOK_AUTHOR": "Tandi Hendrika",
                        "BOOK_DATE": "20 April 1853",
                        "BOOK_DESCRIPTION": null,
                        "BOOK_ID": 0,
                        "BOOK_LANGUAGE": "German",
                        "BOOK_NAME": "Uravan Jaynell",
                        "BOOK_PATH": null
                    },{
                        "BOOK_AUTHOR": "Lenora Eddi",
                        "BOOK_DATE": "16 November 1840",
                        "BOOK_DESCRIPTION": null,
                        "BOOK_ID": 0,
                        "BOOK_LANGUAGE": "German",
                        "BOOK_NAME": "Beatty Jobey",
                        "BOOK_PATH": null
                    },{
                        "BOOK_AUTHOR": "Dorelle Melba",
                        "BOOK_DATE": "15 December 1899",
                        "BOOK_DESCRIPTION": null,
                        "BOOK_ID": 0,
                        "BOOK_LANGUAGE": "Chinese",
                        "BOOK_NAME": "Beals Rianon",
                        "BOOK_PATH": null
                    },{
                        "BOOK_AUTHOR": "Idalina Timothea",
                        "BOOK_DATE": "16 September 1843",
                        "BOOK_DESCRIPTION": null,
                        "BOOK_ID": 0,
                        "BOOK_LANGUAGE": "German",
                        "BOOK_NAME": "Payneville Pat",
                        "BOOK_PATH": null
                    },{

                        "BOOK_AUTHOR": "Mareah Carlen",
                        "BOOK_DATE": "13 October 1845",
                        "BOOK_DESCRIPTION": null,
                        "BOOK_ID": 0,
                        "BOOK_LANGUAGE": "Chinese",
                        "BOOK_NAME": "Hagerstown Isabelle",
                        "BOOK_PATH": null
                    },{
                        "BOOK_AUTHOR": "Danna Lida",
                        "BOOK_DATE": "14 July 1929",
                        "BOOK_DESCRIPTION": null,
                        "BOOK_ID": 0,
                        "BOOK_LANGUAGE": "English",
                        "BOOK_NAME": "Bandana Layney",
                        "BOOK_PATH": null
                    },{
                        "BOOK_AUTHOR": "Gabriell Colleen",
                        "BOOK_DATE": "11 May 1998",
                        "BOOK_DESCRIPTION": null,
                        "BOOK_ID": 0,
                        "BOOK_LANGUAGE": "English",
                        "BOOK_NAME": "Doddsville Cornelia",
                        "BOOK_PATH": null
                    },{
                        "BOOK_AUTHOR": "Deborah Aurora",
                        "BOOK_DATE": "16 November 1889",
                        "BOOK_DESCRIPTION": null,
                        "BOOK_ID": 0,
                        "BOOK_LANGUAGE": "English",
                        "BOOK_NAME": "Bergenfield Robina",
                        "BOOK_PATH": null
                    }
                ];

                for (var i = 0; i < $scope.books.length; i++) {
                    var book = $scope.books[i];

                    book.color = getColorRGB([255,255,255])

                }
            })
            .error(function (err) {
                return err;
            });

    }

}]);